/*
  * Represents board positions. 
 */
class BoardPosition implements Comparator<BoardPosition> {

  int row, column; 
  
  BoardPosition() { }

  BoardPosition(int column, int row) {
    this.row = row;
    this.column = column;
  }

  public int getRowPos() {
    return row;
  }

  public int getColumnPos() {
    return column;
  }

  // Used for debugging, prints its position. 
  public void printPositions() {
    print("BP is " + this.getColumnPos() + "," + this.getRowPos() + "\n");
  }

  // Used to compare board position classes.
  // Returns true if the BoardPositions match. 
  public boolean positionsMatch(BoardPosition bp) {

    if (this.getColumnPos() == bp.getColumnPos() && this.getRowPos() == bp.getRowPos()) {
      // print("POSMATCH" + this.getColumnPos() + bp.getColumnPos() + this.getRowPos() + bp.getRowPos() + "\n"); 
      return true;
    } else { 
      return false;
    }
  }

  public int compare(BoardPosition b1, BoardPosition b2)
  {
    String b1String = "" + b1.getColumnPos() + b1.getRowPos();
    String b2String = "" + b2.getColumnPos() + b2.getRowPos();
    return b1String.compareTo(b2String);
  }
}

