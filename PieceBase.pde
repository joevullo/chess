/**
 * The base class for all the pieces in the game.
 */
abstract class PieceBase {

  protected boolean firstMove = true;

  protected int       cGridPos, rGridPos, //These represent a grid position not, X & Y of the board. 
  xPos, yPos, // X & Y for the actual position of the piece. 
  size = Utils.SQUARE_SIDE, // The size of the squares, we should be drawing the piece based on this. 
  numberOfSquares = Utils.NUM_SQUARES;

  protected int       playerOwner = 0; // Default is nobody?
  protected PImage    pieceImage;
  protected int       boardX, boardY;   
  protected String    pieceColorID; // Should be one of the ID's from Utils i.e Utils.C_PIECE_WHITE_ID.

  // Direction variables.
  private int         dX = 0, dY = 0, 
  speedMod = 5;

  //private int         newPositionR, newPositionC;  

  // These areas represent the available directions.
  // Up, Down, Left, Right, DiaUpLeft , DiaUpRight, DiaDownLeft , DiaDownRight,  
  protected int directionsY[] = {
    0
  };
  protected int directionsX[] = {
    0
  };

  public PieceBase(int cGridPos, int rGridPos, String pieceColorID) { 
    this.cGridPos = cGridPos;
    this.rGridPos = rGridPos;
    this.xPos = rGridPos * size; 
    this.yPos = cGridPos * size;
    this.pieceColorID = pieceColorID;

    setPieceImage();
  }

  public String getPieceColorID() {
    return pieceColorID;
  }

  public boolean hasMoved() {
    return !firstMove;
  }

  protected void setFirstMoveRun() {
    firstMove = false;
  }

  public BoardPosition getBoardPosition() {
    return new BoardPosition(cGridPos, rGridPos);
  }

  public ArrayList<BoardPosition> getPossibleMovesAsList(PieceBase[][] boardPieces) {
    ArrayList<BoardPosition> possibleMovesReturn = new ArrayList<BoardPosition>();
    BoardPosition[] a = getPossibleMoves(boardPieces);

    for (int i = 0; i < a.length; i++) {
      possibleMovesReturn.add(a[i]);
    }

    return possibleMovesReturn;
  }

  // These need to overriden in the subclasses. 
  protected abstract void setPieceImage();


  // Because these moves are relatively the same for each piece they now exist here.
  // The pawn, king & knight have their own implementations of this move since they 
  // require a little bit of different logic.

  /*
   * Returns the moves which are can result in a piece being taken, which are the normal for all pieces
   * except the pawn, which can move forward, but not take a piece from doing so. PawnPiece has 
   * an overriden method for this move.
   */
  public ArrayList<BoardPosition> getKingLethalMoves(PieceBase[][] boardPieces) {
    ArrayList<BoardPosition> moves = new ArrayList<BoardPosition>();
    moves = getLinearMovesUsingDirectionForKing(directionsY, directionsX, boardPieces);
    return moves;
  }

  /*
   * This method returns the moves that the piece concerned with can move without any restrictions. 
   * This should be overriden by its sub classes and implemented according to the piece in question. 
   */
  public ArrayList<BoardPosition> getMoves(PieceBase[][] boardPieces) {
    ArrayList<BoardPosition> moves = new ArrayList<BoardPosition>();
    moves = getPossibleLinearMovesUsingDirection(directionsY, directionsX, boardPieces);
    return moves;
  }

  public ArrayList<BoardPosition> getWholeBoardLethalMoves(PieceBase[][] boardPieces) {
    ArrayList<BoardPosition> oppPossibleMoves = new ArrayList<BoardPosition>();

    // For the whole board. 
    for (int i = 0; i < boardPieces.length; i++) {
      for (int j = 0; j < boardPieces.length; j++) {
        // If its pieces are the opponents.
        if (boardPieces[i][j] != null) {
          // print("i="+ i + " j="+j+"\n");
          if (!boardPieces[i][j].getPieceColorID().equals(pieceColorID)) {
            oppPossibleMoves.addAll(boardPieces[i][j].getKingLethalMoves(boardPieces));
          }
        }
      }
    }

    return oppPossibleMoves;
  }

  // This being called, but the array's that manage the position should also be updated to. 
  public void updatePosition(int newCGridPos, int newRGridPos) {
    rGridPos = newRGridPos; 
    cGridPos = newCGridPos;
  }

  // This computes the direction values if a new position has been added.   
  public void computeDirection() {

    if ((rGridPos * size) > xPos) {
      xPos += speedMod;
    } else if ((rGridPos * size) < xPos) {
      xPos -= speedMod;
    } else if ((rGridPos * size) == yPos) {
      //xPos = 0;
    }

    if ((cGridPos * size) > yPos) {
      yPos += speedMod;
    } else if ((cGridPos * size) < yPos) {
      yPos -= speedMod;
    } else {
      //yPos = 0;
    }
  }

  public void Draw() {
    if (pieceImage != null) { 
      // print("xC " + xC + " yC " + yC + " size " + size + "\n");
      // X & Y of images are the top left corner of the image. 
      computeDirection();
      image(pieceImage, (xPos += dX), (yPos + dY), size, size);
    } else {
      //print("Chess Piece image was null!");
    }
  }

  // Checks a variable to see if its within the bounds of the board. 
  private boolean exceedsBounds(int i) {  
    if (i >=0 && i < numberOfSquares) {
      // print("Move not valid\n");
      return false;
    } else {
      return true;
    }
  }  

  // Checks to see if that the piece is moved to is.  
  protected boolean validBoardMove(int i, int j) {  
    if (!exceedsBounds(i) && !exceedsBounds(j)) {
      return true;
    } else {
      return false;
    }
  }

  // Removes elements from the array list provided if they are beyond the bounds of the board. 
  // This is reallly used just as insurance. 
  protected ArrayList<BoardPosition> removeInvalidBoardMoves(ArrayList<BoardPosition> possibleMoves) {  
    ArrayList<BoardPosition> validPossibleMoves = new ArrayList<BoardPosition>(); 

    for (BoardPosition p : possibleMoves) {
      if (validBoardMove(p.getColumnPos(), p.getRowPos())) {
        validPossibleMoves.add(p);
      }
    }

    return validPossibleMoves;
  }

  // This method gets the board positions by for looping around in all the directions supplied and returning
  // the results. This method will include the first piece of the opponents pieces in the direction that its  
  // heading in. This method can be used by the ook, bishop and queen, other pieces will require slightly different logic. 
  protected ArrayList<BoardPosition> getPossibleLinearMovesUsingDirection(int directionsY[], int directionsX[], PieceBase[][] boardPieces) {

    ArrayList<BoardPosition> possibleMoves = new ArrayList<BoardPosition>();

    //Have another for loop and variables with an array of possible pieces?
    for (int j = 0; j < directionsY.length; j++) {
      boolean firstOppPiece = false;
      for (int i = 1; i < Utils.NUM_SQUARES; i++) {
        // We need to check that its a valid position before we check the board. 

        int newX = rGridPos+(directionsX[j]*i);
        int newY = cGridPos+(directionsY[j]*i);

        if (validBoardMove(newY, newX)) {
          if (boardPieces[newY][newX] == null) {
            possibleMoves.add(new BoardPosition(newY, newX));
          } else if (!boardPieces[newY][newX].getPieceColorID().equals(pieceColorID) && !firstOppPiece) {
            possibleMoves.add(new BoardPosition(newY, newX));
            firstOppPiece = true;
            break;
          } else {
            break;
          }
        }
      }
    }

    return possibleMoves;
  }


  // Gets all moves, not stopping at the first piece when it is a king piece it finds so that king so that it can test its appropiate move correctly.  
  protected ArrayList<BoardPosition> getLinearMovesUsingDirectionForKing(int directionsY[], int directionsX[], PieceBase[][] boardPieces) {

    ArrayList<BoardPosition> possibleMoves = new ArrayList<BoardPosition>();

    //Have another for loop and variables with an array of possible pieces?
    for (int j = 0; j < directionsY.length; j++) {
      boolean firstOppPiece = false;
      for (int i = 1; i < Utils.NUM_SQUARES; i++) {
        // We need to check that its a valid position before we check the board. 

        int newX = rGridPos+(directionsX[j]*i);
        int newY = cGridPos+(directionsY[j]*i);

        if (validBoardMove(newY, newX)) {
          if (boardPieces[newY][newX] == null) {
            possibleMoves.add(new BoardPosition(newY, newX));
          } else if (/*!boardPieces[newY][newX].getPieceColorID().equals(pieceColorID) && */ !firstOppPiece) {
            possibleMoves.add(new BoardPosition(newY, newX));

            // its position we are evaluating is a king piece we don't want the king to be able to think it can move in the positions past it. 
            if (!(boardPieces[newY][newX] instanceof KingPiece)) {
              firstOppPiece = true;
              break;
            }
          } else {
            break;
          }
        }
      }
    }

    return possibleMoves;
  }

  public boolean isThisPieceCausingCheck(KingPiece kp, PieceBase[][] boardPieces, ArrayList<BoardPosition> possibleMoves) {

    // If its null then grab it yourself. This is to avoid an infinite loop when trying to use isThisPieceCausingCheck in getPossibleMovesAsList
    // Its also used in working out check break.
    if (possibleMoves == null) {
      possibleMoves = getPossibleMovesAsList(boardPieces);
    } 

    for (BoardPosition p : possibleMoves) {
      //print(p.getColumnPos() + "" +  p.getRowPos() + kp.getBoardPosition().getRowPos() + "" +  kp.getBoardPosition().getColumnPos() + "\n");
      if (p.getRowPos() == kp.getBoardPosition().getRowPos() && p.getColumnPos() == kp.getBoardPosition().getColumnPos()) {
        return true;
      }
    }    
    return false;
  }

  public boolean listContainsPosition(BoardPosition bp, ArrayList<BoardPosition> list) {
    for (BoardPosition e : list) {
      if (e.positionsMatch(bp)) {
        return true;
      }
    }

    return false;
  }

  //
  public ArrayList<BoardPosition> getBoardPositionsBetween(BoardPosition beginPiece, KingPiece kp, PieceBase[][] boardPieces) {

    ArrayList<BoardPosition> possibleCheckBreakMoves = new ArrayList<BoardPosition>();
    int directionX = 0, directionY = 0;
    int rGridPos = beginPiece.getRowPos();
    int cGridPos = beginPiece.getColumnPos(); 

    //But directions are only valid for some pieces, kings, pawns & knights don't use them and have set pieces. 
    // therefore the logic follows that the only way to break check for these is if they can be taken themselves or 
    // if the king can be moved from it. 
    if (this instanceof PawnPiece || this instanceof KingPiece || this instanceof KnightPiece) {
      // This instanceof stuff is pretty naughty, really I should use inhertiance but this is less code..
      return possibleCheckBreakMoves;
    }

    // First lets work out the direction that we have to go in.
    // We can work out the direction by comparing the position of this piece on the grid
    // vs the piece of the king.  
    if (rGridPos >  kp.getBoardPosition().getRowPos()) {
      // King is right from this piece
      directionX = -1;
    } else if (rGridPos ==  kp.getBoardPosition().getRowPos()) {
      directionX = 0;
    } else {
      // King is left from this piece.
      directionX = +1;
    }

    //This works out the Y direction.       
    if (cGridPos > kp.getBoardPosition().getColumnPos()) {
      // King is down from this piece
      directionY = -1;
    } else if (cGridPos ==  kp.getBoardPosition().getColumnPos()) {
      directionY = 0;
    } else {
      // King is up from this piece.
      directionY = +1;
    }

    // Now lets loop in right direction until we reach the king.
    for (int i = 1; i < Utils.NUM_SQUARES; i++) {
      // We need to check that its a valid position before we check the board. 

      int newX = rGridPos+(directionX*i);
      int newY = cGridPos+(directionY*i);

      if (validBoardMove(newY, newX)) {
        // Keep add the postion unless this matches the positon of the king 
        // piece, but don't include the king piece
        //BoardPosition currentBP = boardPieces[newY][newX].getBoardPosition();
        BoardPosition currentBP = new BoardPosition(newY, newX);
        if (currentBP.getRowPos() == kp.getBoardPosition().getRowPos() && currentBP.getColumnPos() == kp.getBoardPosition().getColumnPos()) {
          break;
        } 
        possibleCheckBreakMoves.add(new BoardPosition(newY, newX));
      }
    }

    return possibleCheckBreakMoves;
  }


  // This method basically computes whether a check condition is being caused, 
  // and returns a list of the possible moves that would be valid to break the check against the king.    
  // if it 0 elements, then check has no been achieved. 

  // This method will produce the break moves for the piece it is called for. 
  public ArrayList<BoardPosition> getPossibleCheckBreakPositions(KingPiece kp, PieceBase[][] boardPieces, ArrayList<BoardPosition> possibleMoves) {
    ArrayList<BoardPosition> possibleCheckBreakMoves = new ArrayList<BoardPosition>();

    //This is the problem. 
    if (!isThisPieceCausingCheck(kp, boardPieces, possibleMoves)) {
      // print(cGridPos + " NO," + rGridPos + " ---" + "\n");
      // We don't have a check condition so return 0 items. 
      return possibleCheckBreakMoves;
    } else {
      print("Y?\n");
      // We do have a check condition, so lets get the direction and get an array list of possible moves to.
      possibleCheckBreakMoves.add(new BoardPosition(cGridPos, rGridPos)); //If there is a check condition, taking the current piece will always break check. 
      BoardPosition a = new BoardPosition(cGridPos, rGridPos);
      possibleCheckBreakMoves.addAll(getBoardPositionsBetween(a, kp, boardPieces));
      return possibleCheckBreakMoves;
    }
  }

  // This is also used to check for the check conditions.  
  public BoardPosition[] getPossibleMoves(PieceBase[][] boardPieces) {

    ArrayList<BoardPosition> possibleMoves = new ArrayList<BoardPosition>();    
    possibleMoves = getMoves(boardPieces); 
    possibleMoves = removeInvalidBoardMoves(possibleMoves);

    String oppPlayerColorID = ""; 
    // If we are in check then we can only make moves that would resolve the check.  
    if (pieceColorID.equals(Utils.C_PIECE_BLACK_ID)) {
      oppPlayerColorID = Utils.C_PIECE_WHITE_ID;
    } else {
      oppPlayerColorID = Utils.C_PIECE_BLACK_ID;
    }

    KingPiece king = (KingPiece) getKingPositionByColor(pieceColorID, boardPieces);
    //The piece needs to know what moves can break the king's problem,

    if (king != null) { 
      if (king.isThisInCheck()) {
        // TODO, this causing a bug. 
        possibleMoves = removeMovesCantBreakCheck(possibleMoves, king, boardPieces);
      } else {
        // TODO
        //Some moves may cause a check condition to come about, these cannot be allowed. 
        //possibleMoves = removeMovesThatCauseCheck(possibleMoves, king, boardPieces);
      }
    } else {
      print("Error: THE KING IS DEAD\n");
    }

    //Remove any pieces that might be
    ArrayList<BoardPosition> invalidMoves = new ArrayList<BoardPosition>();
    // Remove invalid pieces
    for (BoardPosition p : possibleMoves) {
      if (validBoardMove(p.getColumnPos(), p.getRowPos())) {
        if (boardPieces[p.getColumnPos()][p.getRowPos()] != null) {
          if (boardPieces[p.getColumnPos()][p.getRowPos()].getPieceColorID().equals(pieceColorID)) {
            invalidMoves.add(p);
          }
        }
      }
    }

    // Remove invalid pieces
    for (BoardPosition p : invalidMoves) {
      possibleMoves.remove(p);
    }


    // Lets get this back in an array because it uses a lot less memory.
    return possibleMoves.toArray(new BoardPosition[possibleMoves.size()]);
  }

  public void testListContents(ArrayList<BoardPosition> list) {
    //print(cGridPos + "," + rGridPos + "\n");

    for (BoardPosition l : list) {
      l.printPositions();
    }
  }

  public ArrayList<BoardPosition> removeMovesThatCauseCheck(ArrayList<BoardPosition> possibleMoves, KingPiece king, PieceBase[][] boardPieces) {
    ArrayList<BoardPosition> moves = new ArrayList<BoardPosition>();

    for (BoardPosition bp : possibleMoves) {
      // will move cause check.
      if (willMoveCauseCheck(bp, new BoardPosition(cGridPos, rGridPos), king, boardPieces)) {
        moves.add(bp);
      }
    }    

    // If not add that position from the new arraylist
    // Restore the board to its original state and repeat.

    // Return the new arraylist.
    return moves;
  }

  //Temp adjusts board to test wether a check condition will result if the board position
  private boolean willMoveCauseCheck(BoardPosition newMove, BoardPosition originalPos, KingPiece king, PieceBase[][] boardPieces) {

    boolean failedCheck = false;
    PieceBase tempPiece = null; 

    // Adjust the board as the move has been made.
    if (boardPieces[newMove.getColumnPos()][newMove.getRowPos()] != null) {
      tempPiece = boardPieces[newMove.getColumnPos()][newMove.getRowPos()];
    }

    boardPieces[newMove.getColumnPos()][newMove.getRowPos()] = boardPieces[originalPos.getColumnPos()][originalPos.getRowPos()]; 
    boardPieces[originalPos.getColumnPos()][originalPos.getRowPos()] = null;

    // Check that there is or isn't a king in check
    if (doesTheBoardHaveCheck(boardPieces, king)) {
      failedCheck = true;
    } else {
      failedCheck = false;
    }

    //Return the board back to normal.
    boardPieces[originalPos.getColumnPos()][originalPos.getRowPos()] = boardPieces[newMove.getColumnPos()][newMove.getRowPos()];
    if (tempPiece != null) {
      boardPieces[newMove.getColumnPos()][newMove.getRowPos()] = tempPiece;
    }  

    return failedCheck;
  }

  public boolean doesTheBoardHaveCheck(PieceBase[][] boardPieces, KingPiece king) {
    ArrayList<BoardPosition> possibleMoves = new ArrayList<BoardPosition>();
    BoardPosition kingbp = king.getBoardPosition();

    for (int i = 0; i < Utils.NUM_SQUARES; i++) {
      for (int j = 0; j < Utils.NUM_SQUARES; j++) {
        // Can't be null as we'll have some issues. 
        if (boardPieces[i][j] != null) {
          // Check by getting the opps possible positions. 
          if (!boardPieces[i][j].getPieceColorID().equals(getPieceColorID())) {

            // Get all the possible for each piece and add them to the main array.     
            possibleMoves = boardPieces[i][j].getPossibleMovesAsList(boardPieces);

            //Do any of these moves match the position, if they don't then a king won't be marked as check. 
            for (BoardPosition p : possibleMoves) {
              if (p.getRowPos() == kingbp.getRowPos() && p.getColumnPos() == kingbp.getColumnPos()) {
                return true;
              }
            }
          }
        }
      }
    } 
    return true;
  }

  public ArrayList<BoardPosition> removeMovesCantBreakCheck(ArrayList<BoardPosition> possibleMoves, KingPiece king, PieceBase[][] boardPieces) { 
    ArrayList<BoardPosition> result = new ArrayList<BoardPosition>();
    ArrayList<BoardPosition> breakMoves;

    BoardPosition cp = king.getCheckingPiecePosition();

    if (cp == null) {
      return possibleMoves;
    }

    breakMoves = boardPieces[cp.getColumnPos()][cp.getRowPos()].getPossibleCheckBreakPositions(king, boardPieces, null);

    //print(cp.getColumnPos() + "," + cp.getRowPos()+ " -+- \n");
    //testListContents(breakMoves);
    //testListContents(possibleMoves);

    // If any match. 
    for (BoardPosition a : possibleMoves) {
      for (BoardPosition b : breakMoves) {
        if (a.positionsMatch(b)) {
          result.add(a);
        }
      }
    }

    return result;
  }

  // Use the parameter to return a specfic color king i.e Utils.C_PIECE_BLACK_ID
  private PieceBase getKingPositionByColor(String colorID, PieceBase boardPieces[][]) {
    for (int i = 0; i < Utils.NUM_SQUARES; i++) {
      for (int j = 0; j < Utils.NUM_SQUARES; j++) {
        if (boardPieces[i][j] != null) {
          if (boardPieces[i][j] instanceof KingPiece
            && boardPieces[i][j].getPieceColorID().equals(colorID)) { 
            return boardPieces[i][j];
          }
        }
      }
    }

    return null;
  }
}

