/**
 * Contains buttons that are displayed to the user.
 * Such as the current players turn. 
 */
class ControlPanel {

  int cpXPos, cpYPos; // X & Y positions of the start for the control panel.

  int controlPanelSize = 60; //Default is 60.  
  int canvasWidth; 

  Button resetGameButton, 
    playerInfoPanel, 
    cancelSelectionButton; //Not really a button it displays the current player but ah well. 

  //Update this and the control panel will ATTEMPT to scale itself a appropiately to the number of buttons.
  int numberOfButtons = 3;  // Horizontal buttons, that is.  
  
  ControlPanel(int xPos, int yPos, int controlPanelSize, int canvasWidth) { 
    this.cpXPos = xPos; 
    this.cpYPos = yPos;
    this.controlPanelSize = controlPanelSize;
    this.canvasWidth = canvasWidth; 
    setupButtons();
  }

  // This basically allows you to access the inner from the man class and run things 
  // such as "valid button press" etc. 
  public Button getButton(String buttonID) {
    if (buttonID.equals(Utils.RESET_ID)) {
      return resetGameButton;
    } else if (buttonID.equals(Utils.CANCEL_SELECTION)) {
      return cancelSelectionButton;
    } else if (buttonID.equals(Utils.PLAYER_INFO)) {
      return playerInfoPanel;
    } else {
      print("Error retrieving a button");
      return null;
    }
  }

  private void setupButtons() {

    // The available width of the canvas divided by the number of buttons + the orginal start position of the canvas X.
    int button2XStart = cpXPos + (canvasWidth/numberOfButtons);
    int buttonWidth = canvasWidth/numberOfButtons; //Should scale in case of extra button additions. 
    int buttonHeight = controlPanelSize; 
    int button3XStart = cpXPos + ((canvasWidth/numberOfButtons)*2) ;

    // Remember a Button is made up of int xC, int yC, int wSize, int hSize, color fillCol
    playerInfoPanel = new Button(cpXPos, cpYPos, buttonWidth, buttonHeight, Utils.WHITE); 
    resetGameButton = new Button(button2XStart, cpYPos, buttonWidth, buttonHeight, Utils.WHITE);
    cancelSelectionButton = new Button(button3XStart, cpYPos, buttonWidth, buttonHeight, Utils.WHITE);

    playerInfoPanel.setText("Player Info"); 
    resetGameButton.setText("Reset Game");
    cancelSelectionButton.setText("Cancel Selection");
  }

  void Draw() {   
    playerInfoPanel.Draw();
    resetGameButton.Draw();
    cancelSelectionButton.Draw();
  }
}

