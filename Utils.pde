// A static class for constant variables across the program. 
static class Utils {
  
  public static final boolean DEBUG = false;
  
  // TODO, make these match with...
  public static final int SQUARE_SIDE = 70, //Side of individual squares
  NUM_SQUARES = 8, // Number of squares per side
  CONTROL_PANEL_SIZE = 100; 

  public static final int CANVAS_SIDE = SQUARE_SIDE * NUM_SQUARES; // Clearly the width and height of the canvas

  // Used to represent what color piece should be loaded. 
  public static String C_PIECE_BLACK_ID = "BLACK", 
  C_PIECE_WHITE_ID = "WHITE";  

  //ID's for the buttons. 
  public static final String START_ID = "START_ID", 
  RESET_ID = "RESET_ID", 
  PLAYER_INFO = "PLAYER_INFO", 
  CANCEL_SELECTION = "CANCEL_SELECTION";

  // Some generic colors if needed.
  public static color RED = #FF0000, 
  GREEN = #00FF00, 
  BLUE = #0000FF, 
  BLACK = #000000, 
  ORANGE = #FFA000, 
  WHITE = #FFFFFF, 
  GREY = #D3D3C5;
  
  public final static int DIR_ROW_RIGHT = +1,
    DIR_ROW_LEFT = -1;
  
  
  public static String GAME_CHECKMATE = "RESULT_CHECKMATE",
    GAME_CHECK = "RESULT_CHECK",
    GAME_STALEMATE = "STALEMATE", 
    GAME_CONTINUE = "CONTINUE"; 


  public static color SELECTION_COLOR = GREEN;
}
