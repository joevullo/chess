import java.util.*;
/**
 * Chessboard contains the logic to manage the pieces on the board and represent the board.
 * 
 * Chessboards are setup with 8 pawns and then the following order behind:
 *   Rook, Knight, Bishop, King, Queen, Bishop, Knight, Rook. 
 * 
 * Rook pieces that reach the other side of the board can be turnt into other pieces - TODO, consider doing/not doing this. 
 * 
 * A Chessboard is 8 by 8 in size, white always moves first therefore if two players are needed the set needs to alternate. 
 */
class ChessBoard {

  private int     numOfSquares, 
  squareSize;

  private Board   db;
  PieceBase       mPiece;

  // These are arrays of pieces (i.e data about the pieces) which will be mapped against the squares!
  PieceBase[][]   boardPieces;

  // This store the selected positions.
  ArrayList<BoardPosition> selectedPositions = new ArrayList<BoardPosition>();  

  public ChessBoard(int squareSize, int numOfSquares) {
    this.numOfSquares = numOfSquares;
    this.squareSize = squareSize;
    db = new Board(squareSide, numOfSquares);
    boardPieces = new PieceBase[numOfSquares][numOfSquares];
  }

  public void Draw() {
    db.drawBoard();

    for (int i = 0; i < numOfSquares; i++) {
      for (int j = 0; j < numOfSquares; j++) {
        if (boardPieces[i][j] != null) {
          boardPieces[i][j].Draw();
        }
      }
    }
  }

  public void setupChessPieces() {
    defaultPiecePositions();
    //testCheck();
    //pawnPromotionTest();
    //stestCastling();
    //testStalemate();
    //testCheckmate();
  }
  
  public void testCheckmate() {
    
    boardPieces[0][3] = new RookPiece(0, 3, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][7] = new KingPiece(0, 7, Utils.C_PIECE_BLACK_ID);  
    boardPieces[1][2] = new QueenPiece(1, 2, Utils.C_PIECE_BLACK_ID); 
    boardPieces[2][1] = new RookPiece(2, 1, Utils.C_PIECE_BLACK_ID); 

    boardPieces[7][0] = new KingPiece(7, 0, Utils.C_PIECE_WHITE_ID);   
  }

  public void testStalemate() {
    //   Rook, Knight, Bishop, King, Queen, Bishop, Knight, Rook.
    boardPieces[0][0] = new RookPiece(0, 0, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][3] = new KingPiece(0, 3, Utils.C_PIECE_BLACK_ID);  
    //boardPieces[0][4] = new QueenPiece(0, 4, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][7] = new RookPiece(0, 7, Utils.C_PIECE_BLACK_ID); 

    boardPieces[7][0] = new RookPiece(7, 0, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][3] = new KingPiece(7, 3, Utils.C_PIECE_WHITE_ID);
    //boardPieces[7][4] = new QueenPiece(7, 4, Utils.C_PIECE_WHITE_ID);   
    boardPieces[7][7] = new RookPiece(7, 7, Utils.C_PIECE_WHITE_ID);
  }

  public void testCastling() {
    // Setting up the other pieces in this order:

    for (int i = 0; i < numOfSquares; i++) {
      //boardPieces[1][i] = new PawnPiece(1, i, Utils.C_PIECE_BLACK_ID); 
      //boardPieces[6][i] = new PawnPiece(6, i, Utils.C_PIECE_WHITE_ID);
    }

    //   Rook, Knight, Bishop, King, Queen, Bishop, Knight, Rook.
    boardPieces[0][0] = new RookPiece(0, 0, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][3] = new KingPiece(0, 3, Utils.C_PIECE_BLACK_ID);  
    //boardPieces[0][4] = new QueenPiece(0, 4, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][7] = new RookPiece(0, 7, Utils.C_PIECE_BLACK_ID); 

    boardPieces[7][0] = new RookPiece(7, 0, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][3] = new KingPiece(7, 3, Utils.C_PIECE_WHITE_ID);
    //boardPieces[7][4] = new QueenPiece(7, 4, Utils.C_PIECE_WHITE_ID);   
    boardPieces[7][7] = new RookPiece(7, 7, Utils.C_PIECE_WHITE_ID);
  }

  public void pawnPromotionTest() {
    for (int i = 0; i < numOfSquares; i++) {
      boardPieces[1][i] = new PawnPiece(1, i, Utils.C_PIECE_WHITE_ID); 
      boardPieces[6][i] = new PawnPiece(6, i, Utils.C_PIECE_BLACK_ID);
    }

    boardPieces[0][3] = new KingPiece(0, 3, Utils.C_PIECE_BLACK_ID); 
    boardPieces[7][3] = new KingPiece(7, 3, Utils.C_PIECE_WHITE_ID);
  }

  public void defaultPiecePositions() {
    for (int i = 0; i < numOfSquares; i++) {
      //Setup the pawns. 
      // Remember [c][r] 
      boardPieces[1][i] = new PawnPiece(1, i, Utils.C_PIECE_BLACK_ID); 
      boardPieces[6][i] = new PawnPiece(6, i, Utils.C_PIECE_WHITE_ID);
    }

    // Setting up the other pieces in this order:
    //   Rook, Knight, Bishop, King, Queen, Bishop, Knight, Rook.
    boardPieces[0][0] = new RookPiece(0, 0, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][1] = new KnightPiece(0, 1, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][2] = new BishopPiece(0, 2, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][3] = new KingPiece(0, 3, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][4] = new QueenPiece(0, 4, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][5] = new BishopPiece(0, 5, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][6] = new KnightPiece(0, 6, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][7] = new RookPiece(0, 7, Utils.C_PIECE_BLACK_ID); 

    boardPieces[7][0] = new RookPiece(7, 0, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][1] = new KnightPiece(7, 1, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][2] = new BishopPiece(7, 2, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][3] = new KingPiece(7, 3, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][4] = new QueenPiece(7, 4, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][5] = new BishopPiece(7, 5, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][6] = new KnightPiece(7, 6, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][7] = new RookPiece(7, 7, Utils.C_PIECE_WHITE_ID);
  }

  public void testCheck() {

    // Setting up the other pieces in this order:
    //   Rook, Knight, Bishop, King, Queen, Bishop, Knight, Rook.
    boardPieces[0][0] = new RookPiece(0, 0, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][1] = new KnightPiece(0, 1, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][2] = new BishopPiece(0, 2, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][3] = new KingPiece(0, 3, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][4] = new QueenPiece(0, 4, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][5] = new BishopPiece(0, 5, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][6] = new KnightPiece(0, 6, Utils.C_PIECE_BLACK_ID); 
    boardPieces[0][7] = new RookPiece(0, 7, Utils.C_PIECE_BLACK_ID); 

    boardPieces[1][0] = new RookPiece(1, 0, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][1] = new KnightPiece(7, 1, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][2] = new BishopPiece(7, 2, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][3] = new KingPiece(7, 3, Utils.C_PIECE_WHITE_ID); 
    boardPieces[6][3] = new QueenPiece(6, 3, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][5] = new BishopPiece(7, 5, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][6] = new KnightPiece(7, 6, Utils.C_PIECE_WHITE_ID); 
    boardPieces[7][7] = new RookPiece(7, 7, Utils.C_PIECE_WHITE_ID);
  }


  // Called when a square is unselected, this method,
  // just runs through the squares that have been "highlighted" and clears the arraylist. 
  public void squareUnselected(int y, int x) {
    for (int i = 0; i < selectedPositions.size (); i++) { 
      unhighlightSquare(selectedPositions.get(i).getColumnPos(), selectedPositions.get(i).getRowPos());
    }
    selectedPositions.clear();
  }

  public void squareSelected(int y, int x) {
    highlightSquare(y, x);
    BoardPosition possibleMoves[] = boardPieces[y][x].getPossibleMoves(boardPieces);

    // If we are in check, we need to limit the positions available.  
    for (int i = 0; i < possibleMoves.length; i++) { 
      highlightSquare(possibleMoves[i].getColumnPos(), possibleMoves[i].getRowPos());
      selectedPositions.add(possibleMoves[i]);
    }

    selectedPositions.add(new BoardPosition(y, x));
  }

  // Used to highlight specific squares for various reasons, such as currently selected & availble moves.  
  public void highlightSquare(int y, int x) {
    db.highlightSquare(y, x);
  }

  public void unhighlightSquare(int y, int x) {
    db.unhighlightSquare(y, x);
  }

  public boolean isMoveValid(int xPos, int yPos, int newYPos, int newXPos) {   
    // print("Selected: " + newYPos + " " +  newXPos + "\n");
    for (int i = 0; i < selectedPositions.size (); i++) { 
      // print(selectedPositions.get(i).getColumnPos()  + " " + selectedPositions.get(i).getRowPos() + "\n");
      if (selectedPositions.get(i).getColumnPos() == newYPos && selectedPositions.get(i).getRowPos() == newXPos) {
        // print("Correct");
        return true;
      }
    }
    return false;
  }

  public boolean squareHasAPiece(int y, int x) {
    if (boardPieces[y][x] == null)
      return false; 
    else 
      return true;
  }

  public String getSquaresPieceColorID(int y, int x) {
    if (boardPieces[y][x] != null) {
      return boardPieces[y][x].getPieceColorID();
    }

    return "DANGER WILL ROBINSON! DANGER.";
  }

  public void changePiecePosition(int yPos, int xPos, int newYPos, int newXPos) {

    if (boardPieces[yPos][xPos] != null) {

      //TODO, these are the special conditions for castling move.
      if (boardPieces[yPos][xPos] instanceof KingPiece) {

        if (newXPos == xPos-2) {
          KingPiece kp = (KingPiece) boardPieces[yPos][xPos];  
          //Grab the rook
          boardPieces[yPos][xPos-3].updatePosition(newYPos, newXPos+1);
          boardPieces[yPos][xPos-3].setFirstMoveRun(); // Sets the rooks first run.  
          swapPositions(yPos, xPos-3, newYPos, newXPos+1);
        }

        if (newXPos == xPos+2) {
          KingPiece kp = (KingPiece) boardPieces[yPos][xPos];  
          //Grab the rook
          boardPieces[yPos][xPos+4].updatePosition(newYPos, newXPos-1);
          boardPieces[yPos][xPos+4].setFirstMoveRun();   // Sets the rooks first run.
          swapPositions(yPos, xPos+4, newYPos, newXPos-1);
        }
      }

      //Standard moving of the piece. 
      boardPieces[yPos][xPos].updatePosition(newYPos, newXPos); //This updates the draw position. 
      //This rearranges the items in the pieces array aswell.
      swapPositions(yPos, xPos, newYPos, newXPos);
      boardPieces[newYPos][newXPos].setFirstMoveRun();
    }
  }

  public void swapPositions(int yPos, int xPos, int newYPos, int newXPos) {
    PieceBase temp = boardPieces[yPos][xPos];
    boardPieces[yPos][xPos] = null;
    boardPieces[newYPos][newXPos] = temp;
  }

  public boolean canKingMove(String colorID) { 
    PieceBase king = getKingPositionByColor(Utils.C_PIECE_BLACK_ID);
    BoardPosition[] possibleMoves = king.getPossibleMoves(boardPieces);
    // If we have no movies. 
    if (possibleMoves.length == 0) {
      return true;
    }

    return false;
  }

  // This will return a string that represents the condition found, i.e if the game is in stalemate, or check. 
  public String checkGameCondition(Player currentPlayer, Player oppPlayer) {
     
    //print(currentPlayer.getPieceColorID() + " " + oppPlayer.getPieceColorID());
    if (mChessBoard.checkForCheck(currentPlayer.getPieceColorID(), oppPlayer.getPieceColorID())) {
      if (mChessBoard.checkForCheckMate(currentPlayer.getPieceColorID(), oppPlayer.getPieceColorID())) {
        print("CHECKMATE FOUND\n");
        return Utils.GAME_CHECKMATE;
      } else {
       print("GAME CHECK FOUND\n");
        return Utils.GAME_CHECK;
      }
    }
    
    // Finally check for a stalemate. 
    if (mChessBoard.checkBothPlayersForStalemate()) {
      return Utils.GAME_STALEMATE;
    } 
    
    // If we've failed all this then continue the game.   
    return Utils.GAME_CONTINUE;
  }

  // This does so by checking all the board pieces do not have a move that 
  // matches the position of the king piece.
  // You need to pass in the player that you are checking may be in check.
  // Returns true if the player questioned IS in check.   
  public boolean checkForCheck(String currentPlayerId, String oppPlayerColorID) {

    ArrayList<BoardPosition> possibleMoves = getAllPossibleMovesByPlayer(currentPlayerId);
    BoardPosition bp = getKingPositionByColor(oppPlayerColorID).getBoardPosition();

    //Do any of these moves match the position.
    for (BoardPosition p : possibleMoves) {
      if (p.getRowPos() == bp.getRowPos() && p.getColumnPos() == bp.getColumnPos()) {
        return true;
      }
    }

    return false;
  }

  // Possible positions that would break check.
  public ArrayList<BoardPosition> getPossibleBreakCheckMovesForKing(String currentPlayerId, KingPiece king) {
    ArrayList<BoardPosition> possibleBreakMoves = new  ArrayList<BoardPosition>(); 

    //For every piece of the board let loop until we have a possible move that break check. 
    for (int i = 0; i < numOfSquares; i++) {
      for (int j = 0; j < numOfSquares; j++) {    
        // We should only be checking the currentplayers pieces.
        if ( boardPieces[i][j] != null) {   
          if (boardPieces[i][j].getPieceColorID().equals(currentPlayerId)) { 
            ArrayList<BoardPosition> tempPossibleBreakMoves = boardPieces[i][j].getPossibleCheckBreakPositions(king, boardPieces, null);
            // For testing. 
            //print("\nOP = "+ i + "," + j);
            if (tempPossibleBreakMoves.size() != 0) {
              for (BoardPosition bp : tempPossibleBreakMoves) {
                int c = bp.getColumnPos();
                int r = bp.getRowPos();
                //print(" BP " + c + "," + r + " ");
              }
              //End of the testing bit. 
              possibleBreakMoves.addAll(tempPossibleBreakMoves);
            }
          }
        }
      }
    }

    return possibleBreakMoves;
  }

  public void updateKingDetails(Player currentPlayer, Player oppPlayer) {

    ArrayList<BoardPosition> possibleMoves = new ArrayList<BoardPosition>(); 

    KingPiece king = (KingPiece) getKingPositionByColor(oppPlayer.getPieceColorID());
    KingPiece cpKing = (KingPiece) getKingPositionByColor(currentPlayer.getPieceColorID());

    // If we've made another move then we should be ok to clear any possible checks on the kings. 
    // this is ok because if a move has been made then check must have been resolved. 
    cpKing.clearCheckInfo();
    king.clearCheckInfo(); 

    BoardPosition kingbp = king.getBoardPosition();

    for (int i = 0; i < numOfSquares; i++) {
      for (int j = 0; j < numOfSquares; j++) {
        // Can't be null as we'll have some issues. 
        if (boardPieces[i][j] != null) {
          if (boardPieces[i][j].getPieceColorID().equals(currentPlayer.getPieceColorID())) {

            // Get all the possible for each piece and add them to the main array.     
            
            possibleMoves = boardPieces[i][j].getPossibleMovesAsList(boardPieces);

            //Do any of these moves match the position, if they don't then a king won't be marked as check. 
            for (BoardPosition p : possibleMoves) {
              if (p.getRowPos() == kingbp.getRowPos() && p.getColumnPos() == kingbp.getColumnPos()) {
                king.setInCheck(true); 
                king.setCheckingPiecePosition(new BoardPosition(i, j));
                break;
              }
            }
          }
        }
      }
    }
  }

  public void postSucessfulMoveCallback(Player currentPlayer, Player oppPlayer) {
    // Any pawns that have made it to the other side get promoted to queens. 
    promotePotentialPawns(); 

    // Updates the king with the correct details. 
    updateKingDetails(currentPlayer, oppPlayer);
  }

  public void promotePotentialPawns() {

    //For loop the bottom and top rows and check for any pawns in that position, if so change them to queens.
    for (int i = 0; i < Utils.NUM_SQUARES; i++) {

      if (boardPieces[0][i] instanceof PawnPiece) {
        //Assign a new queen. 
        boardPieces[0][i] = new QueenPiece(0, i, boardPieces[0][i].getPieceColorID());
      }

      int lastRow = Utils.NUM_SQUARES-1;

      if (boardPieces[lastRow][i] instanceof PawnPiece) {
        // Assign a new queen of the same color.
        boardPieces[lastRow][i] = new QueenPiece(lastRow, i, boardPieces[lastRow][i].getPieceColorID());
      }
    }
  }

  /**
   * This works by looking at all the pieces of the board, if they are causing the check and then getting all the positions 
   * that can break the check, 
   * If the opps pieces can take one of these positions than check is resolved, if there is more than one then checkmate is also provided.
   * if there are no positions matching opp pieces to possible check breaks from this then checkmate is to be found.  
   *
   */
  public boolean checkForCheckMate(String currentPlayerId, String oppPlayerColorID) {

    ArrayList<BoardPosition> oppPlayerPossibleMoves = getAllPossibleMovesByPlayer(oppPlayerColorID);
    ArrayList<BoardPosition> possibleBreakMoves = new  ArrayList<BoardPosition>(); 

    //getAllPossibleMovesByPlayer(currentPlayerId);

    KingPiece king = (KingPiece) getKingPositionByColor(oppPlayerColorID);

    // If we aren't in check than we cannot be in check, so return false.
    // Or if the king can move, then we def aren't in checkmate and return early with ther result.   
    if (!checkForCheck(currentPlayerId, oppPlayerColorID) || king.getPossibleMoves(boardPieces).length != 0) {
      return false;
    }

    possibleBreakMoves = getPossibleBreakCheckMovesForKing(currentPlayerId, king); 

    // Set sorting & checking for drupes is down by implementing a Comparator in the board position. 
    Set<BoardPosition> noDupSet = new TreeSet<BoardPosition>(new BoardPosition());    
    for (BoardPosition a : oppPlayerPossibleMoves) {
      // The if is just for testing. 
      if (!noDupSet.add(a))
      {
        //print("DUP ");
        //a.printPositions();
      } else {
        //a.printPositions();
      }
    }

    int noOfMatches = 0;
    for (BoardPosition op : noDupSet) {
      for (BoardPosition pb : possibleBreakMoves) {
        if (pb.positionsMatch(op)) {
          noOfMatches++;
        }
      }
    }

    //print("\n" + noOfMatches);

    if (noOfMatches == 0) {
      return true; // Opp can't stop checkmate.
    } else {
      // Anymore and the opp can't make enough moves to counter a checkmate. 
      return false;
    }
  }

  // Will return false if the king can't move or it can't find the king.
  public boolean checkKingCanMove(String colorID) {    
    PieceBase king = getKingPositionByColor(Utils.C_PIECE_WHITE_ID);

    if (king == null) {
      print("Error: No King");
      return false;
    }

    if (king.getPossibleMoves(boardPieces).length == 0) {
      return false;
    }  

    return true;
  }

  // Gets all the possible moves for a specific player using their color. 
  private ArrayList<BoardPosition> getAllPossibleMovesByPlayer(String playerColorId) {
    ArrayList<BoardPosition> possibleMoves = new ArrayList<BoardPosition>();

    for (int i = 0; i < numOfSquares; i++) {
      for (int j = 0; j < numOfSquares; j++) {
        // Can't be null as we'll have some issues. 
        if (boardPieces[i][j] != null) {
          if (boardPieces[i][j].getPieceColorID().equals(playerColorId)) {
            // Get all the possible for each piece and add them to the main array.     
            possibleMoves.addAll(boardPieces[i][j].getPossibleMovesAsList(boardPieces));
            //
          }
        }
      }
    }

    return possibleMoves;
  }

  // Checks both players for stalemate as the current which is to check
  // 
  public boolean checkBothPlayersForStalemate() {
    if (checkForStalemate(Utils.C_PIECE_BLACK_ID) || checkForStalemate(Utils.C_PIECE_WHITE_ID)) {
      return true;
    } else {
      return false;
    }
  }

  // Return true for a stalemate condition. 
  public boolean checkForStalemate(String colorID) {      
    if (getAllPossibleMovesByPlayer(colorID).size() == 0) {
      return true;
    }
    return false;
  }

  // Use the parameter to return a specfic color king i.e Utils.C_PIECE_BLACK_ID
  private PieceBase getKingPositionByColor(String colorID) {
    for (int i = 0; i < numOfSquares; i++) {
      for (int j = 0; j < numOfSquares; j++) {
        if (boardPieces[i][j] != null) {
          if (boardPieces[i][j] instanceof KingPiece
            && boardPieces[i][j].getPieceColorID().equals(colorID)) { 
            //return new BroadPosition(i,j);
            return boardPieces[i][j];
          }
        }
      }
    }

    return null;
  }
}

