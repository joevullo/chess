/**
 * A simple class that just draws a four by four square board. 
 */
class Board {

  int squareSize, numOfSquares;
  Square[][] boardSquares;  

  public Board(int squareSize, int numOfSquares) {
    this.squareSize = squareSize; 
    this.numOfSquares = numOfSquares;

    //boardSquaresSize = squareSize * numOfSquares; // Clearly the width and height of the canvas
    boardSquares = new Square[numOfSquares][numOfSquares];
    setupBoard();
  }

  void setupBoard() {
    int i, j;
    for (i = 0; i < numOfSquares; i++) {
      for (j = 0; j < numOfSquares; j++) {

        color boardSquaresPositionColor = Utils.WHITE; //We need a boardSquares with alternating colors. 

        //When the row is even
        if (i % 2 == 0) {
          //And the column is even. 
          if (j % 2 == 0) {
            boardSquaresPositionColor = Utils.GREY;
          }
        } else {
          // else row is odd, the change the color when the column is odd.
          if (j % 2 != 0) {
            boardSquaresPositionColor = Utils.GREY;
          }
        } 

        boardSquares[j][i] = new Square(i * squareSize + squareSize/2, j * squareSize + 
          squareSize/2, squareSize, boardSquaresPositionColor); //All squares are black
      }
    }
  }

  public void highlightSquare(int y, int x) {
    boardSquares[y][x].highlight();
  }

  public void unhighlightSquare(int y, int x) {
    boardSquares[y][x].unhighlight();
  }

  public void setBoardPiece(int x, int y, PieceBase newPiece) {
    try {
      boardSquares[y][x].setPiece(newPiece);
    } 
    catch(NullPointerException e) {
      print("Null error when setting board piece.");
    }
  }

  public void drawBoard() {
    int i, j;
    for (i = 0; i < numOfSquares; i++) {
      for (j = 0; j < numOfSquares; j++) {      
        boardSquares[i][j].Draw(); // Draw the boardSquares.
      }
    }
  }
}

