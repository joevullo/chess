/**
 * Implements a button. Essentially a view with a text description 
 * and some logic to evaluate whether the piece has been presssed. 
 */
class Button {

  int xC, yC, wSize, hSize;
  String infoText = "Default Text"; 
  color fillCol, 
  White = color(255, 255, 255), 
  Black = color(0); 

  public Button(int xC, int yC, int wSize, int hSize, color fillCol) {
    this.xC = xC;
    this.yC = yC;
    this.wSize = wSize;
    this.hSize = hSize;
    this.fillCol = fillCol;
  }

  public void setText(String newText) {
    infoText = newText;
  }

  public void Draw() {
    //Displaying the rectangle
    fill(fillCol);
    rectMode(CORNER);
    stroke(Black);
    strokeWeight(3);
    rect(xC, yC, wSize, hSize);  

    // Displaying the text 
    fill(Black);
    textSize(20); 
    textAlign(CENTER);
    text(infoText, xC + (wSize/2), yC + hSize/2 + 10);
  }

  // Used to test whether the X & Y falls in the correct range. 
  public boolean buttonPressValid(int xPos, int yPos) {
    //If within the boundaries of the button. 
    if (xC <= xPos && (xC+wSize) >= xPos) {        
      if  (yC <= yPos && (yC+hSize) >= yPos ) {
        return true;
      }
    }  

    //Otherwise return false.
    return false;
  }
}

