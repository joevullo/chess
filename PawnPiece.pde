/*
  * This class implements the pawn piece. 
 * Pawns can move 1 square foward and can take pieces, 1 diagonally to the upper 
 * left or right and then take their place.
 *
 * On their first go pawns can move two places.  
 */
class PawnPiece extends PieceBase {

  private int pieceDirection = 0; // This will be used to move the piece in the relevant direction, 
  // since pawns can only move in one direction.    

  public PawnPiece(int x, int y, String colorID) {
    super(x, y, colorID);

    if (!colorID.equals(Utils.C_PIECE_WHITE_ID) && !colorID.equals(Utils.C_PIECE_BLACK_ID)) {
      print("Error, the colorID was not in the available range.");
    }

    // This should be fine to set the direction that piece can move in
    // by the color, as the color of the pieces doesn't change, the players that own them do.     
    if (colorID.equals(Utils.C_PIECE_WHITE_ID)) { 
      pieceDirection = -1; // White is at the bottom of the board, white pieces must move down then.
    } else {
      pieceDirection = +1;
    }
  }

  protected void setPieceImage() {
    if (pieceColorID.equals(Utils.C_PIECE_WHITE_ID)) {
      pieceImage = loadImage("wpawn.png");
    } else { 
      pieceImage = loadImage("bpawn.png");
    }
  } 

  // Overrides its parents method.  
  public ArrayList<BoardPosition> getKingLethalMoves(PieceBase[][] boardPieces) {
    ArrayList<BoardPosition> lethalMoves = new ArrayList<BoardPosition>();

    // Because we are accessing a board position for this test we need to ensure its valid.
    // Dia right 
    if (validBoardMove(cGridPos+pieceDirection, rGridPos+1) /* && boardPieces[cGridPos+pieceDirection][rGridPos+1] != null */) { //If the board position is filled by a piece, than the pawn can move.
      //if (!boardPieces[cGridPos+pieceDirection][rGridPos+1].getPieceColorID().equals(pieceColorID)) //If the board position is filled by a piece, than the pawn can move.  
      lethalMoves.add(new BoardPosition(cGridPos+pieceDirection, rGridPos+1));
    }

    // Dia left 
    if (validBoardMove(cGridPos+pieceDirection, rGridPos-1) /*&& boardPieces[cGridPos+pieceDirection][rGridPos-1] != null*/) {
      //if (!boardPieces[cGridPos+pieceDirection][rGridPos-1].getPieceColorID().equals(pieceColorID)) //If the board position is filled by a piece, than the pawn can move.
      lethalMoves.add(new BoardPosition(cGridPos+pieceDirection, rGridPos-1));
    }

    return lethalMoves;
  }

  // TODO, fix bug - two moves causes a problem. 
  public ArrayList<BoardPosition> getMoves(PieceBase[][] boardPieces) {
    ArrayList<BoardPosition> moves = new ArrayList<BoardPosition>();

    // Piece specific we manually add the pieces moves on the board.

    // The pawn can always move forward one, except when a another piece is in the way.
    if (validBoardMove(cGridPos+pieceDirection, rGridPos)) { 
      if (boardPieces[cGridPos+pieceDirection][rGridPos] == null) //If the board position is filled by a piece, than the pawn can move.   
        moves.add(new BoardPosition(cGridPos+pieceDirection, rGridPos));
    }

    // If this is the pawns first move, than it can move forward 2.
    if (validBoardMove(cGridPos+(pieceDirection*2), rGridPos)) {
      if (firstMove && boardPieces[cGridPos+(pieceDirection*2)][rGridPos] == null &&
        boardPieces[cGridPos+pieceDirection][rGridPos] == null) /* If there isn't a piece in the way. */ {
        moves.add(new BoardPosition(cGridPos+(pieceDirection*2), rGridPos));
      }
    }

    // Finally, if the pieces diagonally left & right of the pawn are 
    // opponents pieces this can take them.

    // Because we are accessing a board position for this test we need to ensure its valid.
    // Dia right 
    if (validBoardMove(cGridPos+pieceDirection, rGridPos+1) && boardPieces[cGridPos+pieceDirection][rGridPos+1] != null) { //If the board position is filled by a piece, than the pawn can move.
      if (!boardPieces[cGridPos+pieceDirection][rGridPos+1].getPieceColorID().equals(pieceColorID)) //If the board position is filled by a piece, than the pawn can move.  
        moves.add(new BoardPosition(cGridPos+pieceDirection, rGridPos+1));
    }

    // Dia left 
    if (validBoardMove(cGridPos+pieceDirection, rGridPos-1) && boardPieces[cGridPos+pieceDirection][rGridPos-1] != null) {
      if (!boardPieces[cGridPos+pieceDirection][rGridPos-1].getPieceColorID().equals(pieceColorID)) //If the board position is filled by a piece, than the pawn can move.
        moves.add(new BoardPosition(cGridPos+pieceDirection, rGridPos-1));
    }

    return moves;
  }
}

