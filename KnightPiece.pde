/**
 * This class implements the code for the knight pieces. 
 * Knights can move in L shapes and are not constrained by the 
 */
class KnightPiece extends PieceBase {

  public KnightPiece(int x, int y, String colorID) {
    super(x, y, colorID);    
    if (!colorID.equals(Utils.C_PIECE_WHITE_ID) && !colorID.equals(Utils.C_PIECE_BLACK_ID)) {
      print("Error, the colorID was not in the available range.");
    }
  }

  protected void setPieceImage() {
    if (pieceColorID.equals(Utils.C_PIECE_WHITE_ID)) {
      pieceImage = loadImage("wknight.png");
    } else { 
      pieceImage = loadImage("bknight.png");
    }
  }
  
  public ArrayList<BoardPosition> getKingLethalMoves(PieceBase[][] boardPieces) {
    return getMoves(boardPieces);
  }

  public ArrayList<BoardPosition> getMoves(PieceBase[][] boardPieces) {
    ArrayList<BoardPosition> moves = new ArrayList<BoardPosition>();

    // So in every direction, left, right, up, down, we go 2 pieces and then 
    // move +1 or -1 in the opposite horizontal or vetical position..     
    moves.add(new BoardPosition(cGridPos+2, rGridPos-1));
    moves.add(new BoardPosition(cGridPos+2, rGridPos+1));
    moves.add(new BoardPosition(cGridPos-2, rGridPos-1));
    moves.add(new BoardPosition(cGridPos-2, rGridPos+1));

    moves.add(new BoardPosition(cGridPos-1, rGridPos-2));
    moves.add(new BoardPosition(cGridPos+1, rGridPos+2));
    moves.add(new BoardPosition(cGridPos+1, rGridPos-2));
    moves.add(new BoardPosition(cGridPos-1, rGridPos+2));

    return moves;
  }
}

