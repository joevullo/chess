/**
 * The main for the class that handles the setup, drawing and user iteraction by 
 * passing what is needed through multiple classes.
 * 
 * Look here for setup(), draw(), mousePressed() etc.    
 */
private int squareSide = Utils.SQUARE_SIDE, //Side of individual squares
numSquares = Utils.NUM_SQUARES;// Number of squares per side
private int side = squareSide * numSquares; // Clearly the width and height of the canvas
private int controlPanelSize = Utils.CONTROL_PANEL_SIZE; 

private boolean selectionModeOn = false; //Clicking the board enables selection mode.

private String gameCondition = ""; //This variable is used to display to user what their condition the game is in.

// All we need is a chessboard implementation & a control panel. 
private ControlPanel mControlPanel; 
private ChessBoard mChessBoard;

private BoardPosition selected;

private Player player1 = new Player(1, Utils.C_PIECE_WHITE_ID); 
private Player player2 = new Player(2, Utils.C_PIECE_BLACK_ID);

private Player currentPlayer = null; 

void setup() {
  size(side, side + controlPanelSize);
  setupNewChessGame();
}

void setupNewChessGame() {

  // Setup the chessboard and the control panel. 
  mChessBoard = new ChessBoard(squareSide, numSquares);
  mControlPanel = new ControlPanel(0, side, controlPanelSize, side);

  // Sets up the pieces for the initial game. 
  mChessBoard.setupChessPieces();

  // Setting the current player. 
  currentPlayer = player1;
}

void draw() {
  // Setting the current player in the view.
  mControlPanel.getButton(Utils.PLAYER_INFO).setText("Player: " + currentPlayer.getID() + "\n" + gameCondition);

  mControlPanel.Draw();
  mChessBoard.Draw();
}

public void unselect() {
  // mChessBoard.squareUnselected(selected.getColumnPos(), selected.getRowPos());
  mChessBoard.squareUnselected(selected.getColumnPos(), selected.getRowPos());
  selectionModeOn = false;
}

public void select(int y, int x) {
  // This should only highlight squares that are not empty. 
  selectionModeOn = true;
  selected = new BoardPosition(y, x);
  //mChessBoard.highlightSquare(y, x);
  mChessBoard.squareSelected(y, x); // TODO, complete this.
}

//Should be called after a move has move, this will check.  
void postSuccesfulMove() {

  gameCondition = "";

  String result = mChessBoard.checkGameCondition(currentPlayer, getOppPlayer());
  //String result = mChessBoard.checkGameCondition(getOppPlayer(), currentPlayer);
  
  print("RESULT EQUALS \n");
  print(result);

  if (result.equals(Utils.GAME_STALEMATE)) {
    //javax.swing.JOptionPane.showMessageDialog(null, "Gameover, Stalemate has been found.");
    gameCondition = "Stalemate Found";
  } else if (result.equals(Utils.GAME_CHECK)) {
    gameCondition = "Check";
  } else if (result.equals(Utils.GAME_CHECKMATE)) {
    //javax.swing.JOptionPane.showMessageDialog(null, "Checkmate, gameover.");
    gameCondition = "Checkmate";
  } else if (result.equals(Utils.GAME_CONTINUE)) {
    gameCondition = "Continue";
    // keep playing.
  } else {
    print("Error: incorrect game condition issue.");
  }
  
  print("\nGAME: " + gameCondition+"\n");

  // Tells the chessboard that a most has been made and to update anything it needs to.  
  mChessBoard.postSucessfulMoveCallback(currentPlayer, getOppPlayer());
  
  switchPlayers(); //Its the other players go now!
}

void mousePressed() {

  int xPressedPos = mouseX/squareSide;
  int yPressedPos = mouseY/squareSide;

  // If the click is within the board side. 
  if (mouseX > 0 && mouseX < side && mouseY > 0 && mouseY < side) {

    if (selectionModeOn) {
      // If move is valid.
      if (mChessBoard.isMoveValid(selected.getColumnPos(), selected.getRowPos(), yPressedPos, xPressedPos)) {
        //Most the piece to a new position.
        if (selected.getColumnPos() == yPressedPos && selected.getRowPos() == xPressedPos) { 
          // Don't run the change piece position if you've clicked the same piece you've already selected.
        } else {
          mChessBoard.changePiecePosition(selected.getColumnPos(), selected.getRowPos(), yPressedPos, xPressedPos);
          postSuccesfulMove();
        }
      }
      unselect();
    } else {
      if (mChessBoard.squareHasAPiece(yPressedPos, xPressedPos)) {        
        if (mChessBoard.getSquaresPieceColorID(yPressedPos, xPressedPos).equals(currentPlayer.getPieceColorID())) { 
          select(yPressedPos, xPressedPos);
        }
      }
    }
  } else {
    // Disable selection mode. 
    if (selectionModeOn) {
      selectionModeOn = false;
    }

    // Checks if the reset button has been pressed.  
    if (mControlPanel.getButton(Utils.RESET_ID).buttonPressValid(mouseX, mouseY)) {
      print("Button 'Reset Game' has been pressed.\n");
      setupNewChessGame();
    }

    if (mControlPanel.getButton(Utils.CANCEL_SELECTION).buttonPressValid(mouseX, mouseY)) {
      unselect();
    }
  }
}

public void switchPlayers() {
  if (currentPlayer.samePlayer(player1)) {
    currentPlayer = player2;
  } else {
    currentPlayer = player1;
  }
}

public Player getOppPlayer() {
  if (currentPlayer.samePlayer(player1)) {
    return player2;
  } else {
    return player1;
  }
}

