/**
 * Implements the bishop piece. 
 * Bishops move diagonally. 
 */
class BishopPiece extends PieceBase {

  // These areas represent the available directions.
  // Up, Down, Left, Right, DiaUpLeft , DiaUpRight, DiaDownLeft , DiaDownRight,  
  int bDirectionsY[] = {
    -1, -1, +1, +1
  };  
  int bDirectionsX[] = { 
    -1, +1, -1, +1
  };

  public BishopPiece(int x, int y, String colorID) {
    super(x, y, colorID);    
    if (!colorID.equals(Utils.C_PIECE_WHITE_ID) && !colorID.equals(Utils.C_PIECE_BLACK_ID)) {
      print("Error, the colorID was not in the available range.");
    }
    
    directionsY = bDirectionsY;
    directionsX = bDirectionsX; 
  }

  protected void setPieceImage() {
    if (pieceColorID.equals(Utils.C_PIECE_WHITE_ID)) {
      pieceImage = loadImage("wbishop.png");
    } else { 
      pieceImage = loadImage("bbishop.png");
    }
  }
  
  //TODO, bishops are broke. 
}

