/*
 * This is the square class for board pieces.
 *
 * Ignore all of that, it now has a bare minimum of functionality. 
 */
class Square {

  int xC, yC, Side;
  PieceBase mPiece; 
  color currentFillCol, defaultFillCol, 
  White = color(255, 255, 255), 
  Black = color(0);

  boolean selected = false; 
  int playerOwned = 0; //0 means empty, 1 means player1, 2 means player2. 

  public Square(int xC, int yC, int Side, color defaultFillCol) {
    this.xC = xC;
    this.yC = yC;
    this.Side = Side;
    this.defaultFillCol = defaultFillCol;
    this.currentFillCol = defaultFillCol;   
    // All squares have an invisable piece which will be set visable. 
    //this.mPiece = new Piece(xC, yC, Side-10, defaultFillCol);
  }

  public void Draw() {
    fill(currentFillCol);
    noStroke();
    rectMode(CENTER);
    rect(xC, yC, Side, Side);
    if (mPiece != null) {
      // print("SASA\n " + mPiece.getPieceID());
      mPiece.Draw();
    }

    // If debug is on show the positions of the squares in their grid. 
    if (Utils.DEBUG) {

      int cGrid = yC / Utils.SQUARE_SIDE;
      int rGrid = xC / Utils.SQUARE_SIDE;

      fill(Black);
      textSize(20); 
      textAlign(CENTER);

      text(cGrid + "," +rGrid, xC, yC);
    }
  }

  public int getPlayerOwner() {
    return playerOwned;
  }

  public boolean pieceIsEmpty() {
    if (mPiece == null)
      return true; 
    else 
      return false;
  } 

  public void highlight() {
    currentFillCol = Utils.SELECTION_COLOR;
  }

  public void unhighlight() {
    currentFillCol = defaultFillCol;
  }

  public PieceBase getPiece() {
    return mPiece;
  }

  public void setPiece(PieceBase newPiece) {
    //TODO, remove this, we don't use this anymore. 
    mPiece = newPiece;
  } 

  public void setPlayerOwned(int playerID) {
    playerOwned = playerID;
  }
}

