/*
  * This implements the queen piece. 
 * Queens can move vertically, horizontally & diagonally in 
 * every direction except for when a piece is in the way. 
 */
class QueenPiece extends PieceBase {

  // These areas represent the available directions.
  // Up, Down, Left, Right, DiaUpLeft , DiaUpRight, DiaDownLeft , DiaDownRight,  
  int qDirectionsY[] = {
    -1, +1, 0, 0, -1, -1, +1, +1
  };  
  int qDirectionsX[] = { 
    0, 0, +1, -1, -1, +1, -1, +1
  };

  public QueenPiece(int x, int y, String colorID) {
    super(x, y, colorID);    
    if (!colorID.equals(Utils.C_PIECE_WHITE_ID) && !colorID.equals(Utils.C_PIECE_BLACK_ID)) {
      print("Error, the colorID was not in the available range.");
    }
    
    directionsY = qDirectionsY;
    directionsX = qDirectionsX;
  }

  protected void setPieceImage() {
    if (pieceColorID.equals(Utils.C_PIECE_WHITE_ID)) {
      pieceImage = loadImage("wqueen.png");
    } else { 
      pieceImage = loadImage("bqueen.png");
    }
  }
}

