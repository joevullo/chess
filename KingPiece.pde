/**
 * Kings can move 1 in every direction except where when they will be placed in check 
 */
class KingPiece extends PieceBase {

  boolean pieceInCheck = false;
  BoardPosition checkingPiecePosition = null; 

  public KingPiece(int x, int y, String colorID) {
    super(x, y, colorID);    
    if (!colorID.equals(Utils.C_PIECE_WHITE_ID) && !colorID.equals(Utils.C_PIECE_BLACK_ID)) {
      print("Error, the colorID was not in the available range.");
    }
  }

  protected void setPieceImage() {
    if (pieceColorID.equals(Utils.C_PIECE_WHITE_ID)) {
      pieceImage = loadImage("wking.png");
    } else { 
      pieceImage = loadImage("bking.png");
    }
  }

  public void setInCheck(boolean newVal) {
    pieceInCheck = newVal;
  }

  // Sets the position that is placing this king in checking
  public void setCheckingPiecePosition(BoardPosition bp) {
    checkingPiecePosition = bp;
  }

  public boolean isThisInCheck() {
    return pieceInCheck;
  }

  // Sets the check boolean and stored position to null. 
  public void clearCheckInfo() {
    pieceInCheck = false;
    checkingPiecePosition = null;
  }

  public BoardPosition getCheckingPiecePosition() {
    return checkingPiecePosition;
  }

  public ArrayList<BoardPosition> getMoves(PieceBase[][] boardPieces) {
    ArrayList<BoardPosition> moves = new ArrayList<BoardPosition>();

    // Up
    moves.add(new BoardPosition(cGridPos-1, rGridPos));
    // Down
    moves.add(new BoardPosition(cGridPos+1, rGridPos));
    // Left
    moves.add(new BoardPosition(cGridPos, rGridPos+1));
    // Right1
    moves.add(new BoardPosition(cGridPos, rGridPos-1));
    // Dia Up Right
    moves.add(new BoardPosition(cGridPos-1, rGridPos+1));
    // Dia Up Left
    moves.add(new BoardPosition(cGridPos-1, rGridPos-1));
    // Dia Down Right
    moves.add(new BoardPosition(cGridPos+1, rGridPos+1));
    // Dia Down Left
    moves.add(new BoardPosition(cGridPos+1, rGridPos-1));

    //Castling moves are now in getPossibleMoves because of their dependency on the current
    //state of the board. 

    return moves;
  }

  public ArrayList<BoardPosition> getKingLethalMoves(PieceBase[][] boardPieces) {
    return getMoves(boardPieces);
  }

  /* There is an current issue with the king, because we do not evaluate the proper positions a king can currently access
   * we may come across a position where the king cannot move against another king when it should be able to if the king is protected 
   * against check in some fashion. The fix for this would be to provide an implementation that stores its possible positions 
   * within the board, but in this case this is a near enough approximation of chess, since this chess logic is not the main point 
   * of this GRAPHICS and ANIMATION course. 
   * 
   */
  public BoardPosition[] getPossibleMoves(PieceBase[][] boardPieces) {
    ArrayList<BoardPosition> possibleMoves = new ArrayList<BoardPosition>();

    possibleMoves = getMoves(boardPieces);

    // This method removes all elements from the board that aren't valid.  
    possibleMoves = removeInvalidBoardMoves(possibleMoves);

    ArrayList<BoardPosition> oppPossibleMoves = getWholeBoardLethalMoves(boardPieces);
    //ArrayList<BoardPosition> oppPossibleMoves  = new ArrayList<BoardPosition>();

    // A king can't move to a place that would put itself in check, so we need to check all the opponents possible positions
    // A then remove the collisions from our kings possible positions.
    ArrayList<BoardPosition> invalidMoves = new ArrayList<BoardPosition>();

    for (BoardPosition p : possibleMoves) {
      for (BoardPosition o : oppPossibleMoves) {
        // If the opp possible moves match the kings, then the king can't move to that position. 
        if (p.getColumnPos() == o.getColumnPos() && p.getRowPos() == o.getRowPos()) {
          invalidMoves.add(p);
        }

        // Kings can't take their own pieces either.
        if (validBoardMove(p.getColumnPos(), p.getRowPos())) {
          if (boardPieces[p.getColumnPos()][p.getRowPos()] != null) {
            if (boardPieces[p.getColumnPos()][p.getRowPos()].getPieceColorID().equals(pieceColorID)) {
              invalidMoves.add(p);
            }
          }
        }
      }
    }


    // Adding the castling. 
    if (leftCastlingAvailable(boardPieces) != null) {
      possibleMoves.add(leftCastlingAvailable(boardPieces));
    } 
    if (rightCastlingAvailable(boardPieces) != null) {
      possibleMoves.add(rightCastlingAvailable(boardPieces));
    }

    // Remove invalid pieces
    for (BoardPosition p : invalidMoves) {
      possibleMoves.remove(p);
    }

    // Lets get this back in an array because it uses a lot less memory. 
    // return validMoves.toArray(new BoardPosition[validMoves.size()]);
    return possibleMoves.toArray(new BoardPosition[possibleMoves.size()]);
    // return validPossibleMoves.toArray(new BoardPosition[validPossibleMoves.size()]);
  }


  // Rules for castling:   
  // * Your king has been moved earlier in the game.
  // * The rook that castles has been moved earlier in the game.
  // * There are pieces standing between your king and rook.
  // * The king is in check.
  // * The king moves through a square that is attacked by a piece of the opponent.
  // * The king would be in check after castling.

  // Returns null if we can't find a position. Returns the position of the rook that we can castle. 
  private BoardPosition castlingAvailable(PieceBase boardPieces[][], int direction) {

    ArrayList<BoardPosition> oppPossibleMoves /*= new ArrayList<BoardPosition>();*/ = getWholeBoardLethalMoves(boardPieces);
    int rookRow = 0;

    // You can't castle if the first move has been made or the piece is in check. 
    if (firstMove == false || pieceInCheck == true) {
      return null;
    } 

    for (int i = 1; i < Utils.NUM_SQUARES; i++) {

      int col = cGridPos; 
      int row = rGridPos+(direction*i); //This pieces row
      //print("We're trying " + col + "," + row + "\n");

      if (validBoardMove(col, row)) {
        PieceBase pb = boardPieces[col][row];
        //If we've found a rook and we've gone more than 3 spaces which is required for castling on a chess board.  
        if (pb instanceof RookPiece && i > 1 && !pb.hasMoved()) {
          //print("We've found " + col + "," + row + "\n");
          return new BoardPosition(col, rGridPos+(direction*2));
          // If the position is null or if any of the positions contain a check.  
        } else if (pb != null || listContainsPosition(new BoardPosition(col,row), oppPossibleMoves)) {
          //print((pb instanceof RookPiece) + " " + (i > 1) + " " + !pb.hasMoved() + " " + !listContainsPosition(new BoardPosition(col,row), oppPossibleMoves));
          //pb.getBoardPosition().printPositions();
          //print("NULL FOUND\n");
          return null; // We should either find the above condition or null spaces.
        }
      }
    }

    print("DANGER\n");
    return null;
  }

  public BoardPosition rightCastlingAvailable(PieceBase boardPieces[][]) {
    return castlingAvailable(boardPieces, Utils.DIR_ROW_RIGHT);
  }

  // TODO, a method that can test whether castling can be performed to the right, and one to the left?
  // Returns null if unavailable and a boardposition if it is available. 
  public BoardPosition leftCastlingAvailable(PieceBase boardPieces[][]) {
    return castlingAvailable(boardPieces, Utils.DIR_ROW_LEFT);
  }
}

