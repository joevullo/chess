/*
  * This implements the Rook piece. 
 * Rooks can move vertically & horizontally. 
 */
class RookPiece extends PieceBase {

  // These areas represent the available directions.
  // Up, Down, Left, Right.  
    private int rDirectionsY[] = {
      -1, +1, 0, 0
    };  
    private int rDirectionsX[] = { 
      0, 0, +1, -1
    };

  public RookPiece(int x, int y, String colorID) {
    super(x, y, colorID);    
    if (!colorID.equals(Utils.C_PIECE_WHITE_ID) && !colorID.equals(Utils.C_PIECE_BLACK_ID)) {
      print("Error, the colorID was not in the available range.");
    }

    directionsY = rDirectionsY;
    directionsX = rDirectionsX; 
  }

  protected void setPieceImage() {
    if (pieceColorID.equals(Utils.C_PIECE_WHITE_ID)) {
      pieceImage = loadImage("wrook.png");
    } else { 
      pieceImage = loadImage("brook.png");
    }
  }
}



